package com.atlassian.security.xml;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import org.junit.Test;
import org.xml.sax.InputSource;

import static com.atlassian.security.xml.SecureXmlParserFactory.newXmlReader;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JaxbBehaviourTest
{
    @Test
    public void unmarshallingWithProvidedXmlReader() throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(Simple.class);

        String xmlForSimpleObject = "<!DOCTYPE root SYSTEM '/no-such-file'> <simple><value>defined-in-xml</value></simple>";

        Unmarshaller um = context.createUnmarshaller();

        Object unmarshalled;

        Source source = new SAXSource(newXmlReader(), new InputSource(new StringReader(xmlForSimpleObject)));
        unmarshalled = um.unmarshal(source);

        assertNotNull(unmarshalled);
        assertEquals("defined-in-xml", ((Simple) unmarshalled).value);
    }
}

@XmlRootElement
class Simple
{
    @XmlElement
    String value = "test";

    @Override
    public String toString()
    {
        return "Simple(" + value + ")";
    }
}
