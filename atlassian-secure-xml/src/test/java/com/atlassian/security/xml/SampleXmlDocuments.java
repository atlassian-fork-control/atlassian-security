package com.atlassian.security.xml;

public class SampleXmlDocuments
{
    public static String BILLION_LAUGHS = "<?xml version='1.0' encoding='utf-8' ?> <!DOCTYPE lolz [ <!ENTITY lol 'lol'> <!ENTITY lol2 '&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;'> <!ENTITY lol3 '&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;'> <!ENTITY lol4 '&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;'> <!ENTITY lol5 '&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;'> <!ENTITY lol6 '&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;'> <!ENTITY lol7 '&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;'> <!ENTITY lol8 '&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;'> <!ENTITY lol9 '&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;'> ]><x>&lol9;</x>";

    private static String FILE_ENTITY = "<!DOCTYPE soapenv:Envelope [<!ENTITY readme SYSTEM '/etc/passwd'>]><x>&readme;</x>";

    private static String FILE_EXTERNAL_PARAMETER_ENTITY = "<!DOCTYPE soapenv:Envelope ["
            + "<!ENTITY % dtd SYSTEM '/etc/passwd'>"
            + "%dtd;"
            + "]><x/>";

    public static String externalResourceEntity()
    {
        return FILE_ENTITY;
    }

    public static String externalResourceEntity(String url)
    {
        return FILE_ENTITY.replaceAll("/etc/passwd", url);
    }

    public static String externalParameterEntity(String url)
    {
        return FILE_EXTERNAL_PARAMETER_ENTITY.replaceAll("/etc/passwd", url);
    }

    public static String EXTERNAL_DTD = "<!DOCTYPE root SYSTEM '/no-such-file'> <root/>";

    private static String EXTERNAL_URL_DTD = "<!DOCTYPE root SYSTEM 'XXX'> <root/>";

    public static String externalUrlDtd(String url)
    {
        return EXTERNAL_URL_DTD.replaceAll("XXX", url);
    }

    public static String AMPERSAND_DOCUMENT = "<x>&amp;</x>";
}
