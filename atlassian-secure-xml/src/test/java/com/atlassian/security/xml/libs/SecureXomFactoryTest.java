package com.atlassian.security.xml.libs;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import com.atlassian.security.xml.HttpAttemptDetector;
import com.atlassian.security.xml.SampleXmlDocuments;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.ParsingException;

import static com.atlassian.security.xml.libs.SecureXomFactory.newBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class SecureXomFactoryTest
{
    @Test
    public void testXomParserIsBroken() throws IOException, ParsingException
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);
        Builder builder = new Builder();
        Document doc = builder.build(in);
        assertEquals("Did not load SYSTEM entity containing TOP SECRET", "TOP SECRET", doc.getValue().trim());
    }

    @Test
    public void testXomParserIsSafe() throws Exception
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);

        Exception ex = null;
        try {
            Builder builder = newBuilder();
            Document doc = builder.build(in);
        }
        catch(ParsingException e)
        {
            ex = e;
        }
        assertNotNull(ex);
        assertEquals("Could not resolve entity readme",ex.getMessage());
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = newBuilder().build(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getValue());
    }

    @Test(expected = ParsingException.class, timeout=1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        newBuilder().build(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
    }

    @Test
    public void externalEntityIsNotIncludedInResults() throws Exception
    {
        Exception ex = null;
        try {
            Document d = newBuilder().build(new StringReader(SampleXmlDocuments.externalResourceEntity()));
        }
        catch(ParsingException e)
        {
            ex = e;
        }
        assertNotNull(ex);
        assertEquals("Could not resolve entity readme",ex.getMessage());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        try {
        newBuilder().build(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));
        }
        catch(ParsingException e)
        {
            assertEquals("Could not resolve entity readme",e.getMessage());
        }
        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = newBuilder().build(new StringReader(s));
        assertEquals("root", d.getRootElement().getLocalName());
        assertEquals(0, d.getRootElement().getChildCount());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
