package com.atlassian.security.xml;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Test;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SaxParserBehaviourTest
{
    static XMLReader createXmlReader() throws ParserConfigurationException, SAXException
    {
        return SecureXmlParserFactory.newXmlReader();
    }

    static XMLReader createNamespaceAwareXmlReader() throws ParserConfigurationException, SAXException
    {
        return SecureXmlParserFactory.newNamespaceAwareXmlReader();
    }

    @Test(expected = SAXNotRecognizedException.class)
    public void askingForUnknownAttributesFails() throws Exception
    {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("no-attribute-with-this-identifier", false);
    }

    static InputSource in(String content) throws UnsupportedEncodingException
    {
        return new InputSource(new ByteArrayInputStream(content.getBytes("us-ascii")));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        XMLReader xr = createXmlReader();

        StringGathererHandler sgh = new StringGathererHandler();

        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.AMPERSAND_DOCUMENT));

        assertEquals("&", sgh.toString());
    }

    static class StringGathererHandler extends DefaultHandler
    {
        private StringBuilder sb = new StringBuilder();

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException
        {
            sb.append(ch, start, length);
        }

        @Override
        public String toString()
        {
            return sb.toString();
        }

        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException
        {
            throw new IOException();
        }
    }

    static class StringGathererContentHandler extends DefaultHandler
    {
        private StringBuilder sb = new StringBuilder();

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException
        {
            sb.append(ch, start, length);
        }

        @Override
        public String toString()
        {
            return sb.toString();
        }
    }

    static class FailingEntityResolver implements EntityResolver
    {
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
        {
            throw new IOException();
        }
    }

    @Test(expected = SAXParseException.class, timeout = 1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        StringGathererHandler sgh = new StringGathererHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.BILLION_LAUGHS));

        assertEquals("", sgh.toString());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        StringGathererHandler sgh = new StringGathererHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", sgh.toString());
    }

    @Test
    public void externalEntityIsNotIncludedInResultUsingXmlReader() throws Exception
    {
        StringGathererContentHandler sgh = new StringGathererContentHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", sgh.toString());
    }

    @Test
    public void externalEntityIsNotReadUsingXmlReader() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        StringGathererContentHandler sgh = new StringGathererContentHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToFile() throws Exception
    {
        StringGathererHandler sgh = new StringGathererHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(SampleXmlDocuments.EXTERNAL_DTD));

        assertEquals("", sgh.toString());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        StringGathererHandler sgh = new StringGathererHandler();

        XMLReader xr = createXmlReader();
        xr.setContentHandler(sgh);
        xr.parse(in(s));

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
        assertEquals("", sgh.toString());
    }
}
