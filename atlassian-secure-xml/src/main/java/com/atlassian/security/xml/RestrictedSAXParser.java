package com.atlassian.security.xml;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * SAXParser which uses a secure (empty) entity resolver, delegates down to the usual SAXParser
 *
 * @since 3.1.3
 */
class RestrictedSAXParser extends SAXParser
{

    private final SAXParser delegate;

    public RestrictedSAXParser(SAXParser inner)
    {
        delegate = inner;
    }

    @Override
    public Parser getParser() throws SAXException
    {
        return delegate.getParser();
    }

    @Override
    public XMLReader getXMLReader() throws SAXException
    {
        final XMLReader innerReader = delegate.getXMLReader();
        innerReader.setEntityResolver(SecureXmlParserFactory.emptyEntityResolver());
        return new RestrictedXMLReader(innerReader);
    }

    @Override
    public boolean isNamespaceAware()
    {
        return delegate.isNamespaceAware();
    }

    @Override
    public boolean isValidating()
    {
        return delegate.isValidating();
    }

    @Override
    public void setProperty(String name, Object value) throws SAXNotRecognizedException, SAXNotSupportedException
    {
        delegate.setProperty(name, value);
    }

    @Override
    public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException
    {
        return delegate.getProperty(name);
    }

    @Override
    public void reset()
    {
        delegate.reset();
    }

    @Override
    public void parse(InputStream is, HandlerBase hb) throws SAXException, IOException
    {
        delegate.parse(is, hb);
    }

    @Override
    public void parse(InputStream is, HandlerBase hb, String systemId) throws SAXException, IOException
    {
        delegate.parse(is, hb, systemId);
    }

    @Override
    public void parse(InputStream is, DefaultHandler dh) throws SAXException, IOException
    {
        delegate.parse(is, dh);
    }

    @Override
    public void parse(InputStream is, DefaultHandler dh, String systemId) throws SAXException, IOException
    {
        delegate.parse(is, dh, systemId);
    }

    @Override
    public void parse(String uri, HandlerBase hb) throws SAXException, IOException
    {
        delegate.parse(uri, hb);
    }

    @Override
    public void parse(String uri, DefaultHandler dh) throws SAXException, IOException
    {
        delegate.parse(uri, dh);
    }

    @Override
    public void parse(File f, HandlerBase hb) throws SAXException, IOException
    {
        delegate.parse(f, hb);
    }

    @Override
    public void parse(File f, DefaultHandler dh) throws SAXException, IOException
    {
        delegate.parse(f, dh);
    }

    @Override
    public void parse(InputSource is, HandlerBase hb) throws SAXException, IOException
    {
        delegate.parse(is, hb);
    }

    @Override
    public void parse(InputSource is, DefaultHandler dh) throws SAXException, IOException
    {
        delegate.parse(is, dh);
    }

    @Override
    public Schema getSchema()
    {
        return delegate.getSchema();
    }

    @Override
    public boolean isXIncludeAware()
    {
        return delegate.isXIncludeAware();
    }
}
