package com.atlassian.security.xml.libs;

import com.atlassian.security.xml.SecureXmlParserFactory;
import org.dom4j.io.SAXReader;

/**
 * A class with a utility method to produce a <a href='http://en.wikipedia.org/wiki/Dom4j'>dom4j</a> parser suitable for untrusted XML.
 *
 * @since 3.0
 */
public class SecureDom4jFactory
{
    private SecureDom4jFactory()
    {
    }

    /**
     * Create a dom4j {@link SAXReader} using {@link SecureXmlParserFactory}, suitable for parsing XML from an untrusted source.
     */
    public static SAXReader newSaxReader()
    {
        SAXReader saxReader = new SAXReader(SecureXmlParserFactory.newXmlReader());
        return saxReader;
    }
}
