#!/bin/bash

# Try to run the tests with different XML parsers

# Get results with:
#  ./run-with-multiple-parsers.sh | tee results.wikimarkup

set -e

echo '||Group||Artifact||Version||Result||'

{
# Dummy - fall back to JDK
echo 'junit junit 4.8.2'

# Xerces
for v in 2.0.0 2.0.2 2.2.1 2.3.0 2.4.0 2.5.0 2.6.0 2.6.1 2.6.2 2.7.1 2.8.0 2.8.1 2.9.1 2.10.0; do
  echo xerces xercesImpl $v
done

# Aelfred
echo 'aelfred aelfred 1.2'

} | while read g a v; do

  echo -n "|$g|$a|$v|"

  if mvn -Dtest.xml.parser.groupId=$g -Dtest.xml.parser.artifactId=$a -Dtest.xml.parser.version=$v clean verify >&2; then
    echo '(/)|'
  else
    echo '(x)|'
  fi
done
