package com.atlassian.security.utils;

import org.junit.Rule;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

@RunWith(Theories.class)
public class ConstantTimeComparisonTest
{
    @DataPoints
    public static final String[] TEST_STRINGS = {"", " ", "☃☃", "\u1337b", "34SDF*(&x"};

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Theory
    public void isEqualStrings(String string) throws Exception
    {
        assertThat(ConstantTimeComparison.isEqual(string, string), is(true));
    }

    @Theory
    public void isEqualStringsOnNotEqualStrings(String string) throws Exception
    {
        final String otherString = "DIFFERENT" + string;
        assertThat(string, not(otherString));
        assertThat(ConstantTimeComparison.isEqual(string, otherString), is(false));
    }

    @Theory
    public void isEqualBytes(String string) throws Exception
    {
        assertThat(ConstantTimeComparison.isEqual(string.getBytes(), string.getBytes()), is(true));
    }

    @Theory
    public void isEqualBytesOnNotEqualBytes(String string) throws Exception
    {
        final String otherString = "DIFFERENT" + string;
        assertThat(string, not(otherString));
        assertThat(ConstantTimeComparison.isEqual(string.getBytes(), otherString.getBytes()), is(false));
    }

    @Theory
    public void isEqualThrowsNullPointerExceptionWhenGivenNull(String string)
        throws Exception
    {
        final String otherString = null;
        assertThat(string, not(otherString));
        thrown.expect(NullPointerException.class);
        ConstantTimeComparison.isEqual(string, otherString);
    }

}
