package com.atlassian.security.csp;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContentSecurityPolicyHeaderGeneratorTest {

    @Test
    public void testBuildCsp() throws Exception {
        String csp = ContentSecurityPolicyHeaderGenerator.buildCsp(ContentSecurityPolicyHeaderFilter.defaultPolicy,"123qweASD");
        assertEquals(csp, "object-src 'none';frame-ancestors 'self';script-src 'unsafe-eval'  'nonce-123qweASD' " +
                " 'unsafe-inline' https: http: 'strict-dynamic';base-uri 'self';report-uri " +
                "https://csp-report-logger.prod.public.atl-paas.net/");
    }

}