package com.atlassian.security;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A servlet filter for adding response headers. Generally, each implementation of a ResponseHeaderFilter can be skipped
 * by setting a request attribute of the form <code>skip!{getHeaderName()}</code>.
 */
public interface ResponseHeaderFilter extends Filter {

    String SKIP_PREFIX = "skip!";

    String getHeaderName();

    String getHeaderFields();

    default void init(FilterConfig filterConfig) throws ServletException {
    }

    default void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                          FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (!response.getHeaderNames().contains(getHeaderName()) && !shouldSkip(servletRequest)) {
            response.setHeader(getHeaderName(), getHeaderFields());
        }
        filterChain.doFilter(servletRequest, response);
    }

    default boolean shouldSkip(ServletRequest servletRequest) {
        Object attr = servletRequest.getAttribute(SKIP_PREFIX + getHeaderName());
        return attr != null && Boolean.TRUE.equals(attr);
    }

    default void destroy() {
    }
}
