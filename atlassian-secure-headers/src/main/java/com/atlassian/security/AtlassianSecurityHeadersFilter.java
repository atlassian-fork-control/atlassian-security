package com.atlassian.security;

import com.atlassian.security.csp.ContentSecurityPolicyHeaderFilter;
import com.atlassian.security.hsts.HTTPStrictTransportSecurityFilter;
import com.atlassian.security.xheaders.ContentTypeOptionsFilter;
import com.atlassian.security.xheaders.FrameOptionsFilter;
import com.atlassian.security.xheaders.XSSProtectionFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Provides all the standard Atlassian security headers:
 * <ul>
 *     <li>Content-Security-Policy / Content-Security-Policy-Report-Only</li>
 *     <li>Strict-Transport-Security</li>
 *     <li>X-XSS-Protection</li>
 *     <li>X-Content-Type-Options</li>
 *     <li>X-Frame-Options</li>
 * </ul>
 * <p>
 * Generally, each of these can be skipped by setting a request attribute of the form <code>skip!{header name}</code>.
 */
@WebFilter(asyncSupported = true)
public class AtlassianSecurityHeadersFilter implements Filter {

    private static List<Filter> filters = Arrays.asList(
            new ContentSecurityPolicyHeaderFilter(),
            new HTTPStrictTransportSecurityFilter(),
            new ContentTypeOptionsFilter(),
            new FrameOptionsFilter(),
            new XSSProtectionFilter()
    );

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        for (Filter filter : filters) {
            filter.init(filterConfig);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        for (Filter filter : filters) {
            // run all filters, but give each an empty filter chain so it doesn't cascade
            filter.doFilter(servletRequest, servletResponse, (request, response) -> {});
        }
        // keep calm and carry on
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
