package com.atlassian.security.xheaders;

import com.atlassian.security.ResponseHeaderFilter;

import javax.servlet.annotation.WebFilter;

/**
 * A simple filter for adding the <code>X-XSS-Protection: 1; mode=block</code> response header to all responses. This
 * helps protect against certain cross-site scripting attacks.
 * <p>
 * Note that the header can be skipped by setting a request attribute of <code>skip!X-XSS-Protection</code>.
 */
@WebFilter(asyncSupported = true)
public class XSSProtectionFilter implements ResponseHeaderFilter {

    @Override
    public String getHeaderName() {
        return "X-XSS-Protection";
    }

    @Override
    public String getHeaderFields() {
        return "1; mode=block";
    }

}
